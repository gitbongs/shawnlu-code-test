const path = require('path');

const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const Dotenv = require('dotenv-webpack');

const MODE = 'production';

module.exports = merge(common, {
    mode: MODE,
    optimization: {
        runtimeChunk: true,
        splitChunks: {
            chunks: 'async',
            minSize: 30000,
            maxSize: 0,
            minChunks: 1,
            maxAsyncRequests: 5,
            maxInitialRequests: 3,
            automaticNameDelimiter: '~',
            name: true,
            cacheGroups: {
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    priority: -10
                },
                default: {
                    minChunks: 2,
                    priority: -20,
                    reuseExistingChunk: true
                }
            }
        }
    },
    plugins: [
        new UglifyJsPlugin({
            sourceMap: true,
            uglifyOptions: {
                ie8: true,
                ecma: 8,
                output: {
                    comments: false,
                },
                compress: {
                    warnings: false,
                    drop_debugger: true,
                    drop_console: true
                },
                warnings: false
            }
        }),
        new HtmlWebpackPlugin({
            // title: 'Production',
            template: './src/index.html',
            hash: true,
            minify: {
                removeAttributeQuotes: true, //删除双引号
                collapseWhitespace: true //折叠 html 为一行
            }
        }),
        new Dotenv({
            path: path.resolve(`./.env.${MODE}`)
        }),
    ]
});