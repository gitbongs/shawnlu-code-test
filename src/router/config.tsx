import Client from "./../pages/Clients/index";

const routesConfig = [
    {
        path: "/",
        exact: true,
        component: Client
    }
    // {
    //     path: "/about",
    //     exact: true,
    //     component: About,
    //     routes: [
    //         {
    //             path: "/tacos/bus",
    //             component: About
    //         },
    //         {
    //             path: "/tacos/cart",
    //             component: About
    //         }
    //     ]
    // }
];

export default routesConfig;
