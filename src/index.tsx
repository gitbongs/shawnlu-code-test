// import "@babel/polyfill";
import * as React from 'react'
import * as ReactDom from 'react-dom';
import {Provider} from 'react-redux';

import './global.scss'
import './global.js'

import App from './App';
import store from './redux/store';


console.log(process.env.DB_PASS, process.env.NODE_ENV);

ReactDom.render(
    <Provider store={store}>
        <App name={'App'} age={1}/>
    </Provider>
    , document.getElementById('app'));
