import * as React from 'react';
import SearchBar from './../../components/SearchBar';
import { connect } from 'react-redux';

import { fetchClientData, filterClient, doDelete, _reset } from "../../redux/action";

const STYLE = require('./style.scss');

const mapState = (state: any) => {
    return {
        clients: state.clients,
        filterResult: state.filterResult,
    };
};

const mapDispatch = (dispatch: any) => {
    return {
        fetchClientData: () => {
            dispatch(fetchClientData());
        },
        filterClient: (val: string) => {
            dispatch(filterClient(val));
        },
        doDelete: (id: string) => {
            dispatch(doDelete(id));
        },
        _reset: () => {
            dispatch(_reset());
        }
    };
};

interface IProps {
    clients: any,
    filterResult: any,
    fetchClientData: any,
    filterClient: any,
    doDelete: any,
    history: any,
    _reset: any
}

class Clients extends React.Component<IProps, {}> {
    constructor(props: IProps) {
        super(props);
    }

    componentDidMount(): void {
        this.props._reset();
    }

    onSearch(val: string) {
        this.props.filterClient(val);
    }

    onEdit(id: string) {
        this.props.history.push(`/client/${id}`);
    }

    onDelete(id: string) {
        this.props.doDelete(id);
    }

    render() {
        const { filterResult } = this.props;
        const clientDOM: any = [];
        filterResult.data.map((item: any, i: number) => {
            clientDOM.push(
                <tr key={i}>
                    <td>{item.first_name}</td>
                    <td>{item.last_name}</td>
                    <td>{item.date_of_birth}</td>
                    <td>
                        <span className={STYLE.actionBtn} onClick={this.onEdit.bind(this, item.id)}>Edit</span>
                        <span> | </span>
                        <span className={STYLE.actionBtn} onClick={this.onDelete.bind(this, item.id)}>Delete</span>
                    </td>
                </tr>
            )
        });
        if (filterResult.data.length === 0) {
            const attr = { colSpan: 4 };
            clientDOM.push(
                <tr key={'no result'}>
                    <td {...attr}>No Result</td>
                </tr>)
        }

        return (
            <div className={`gb-grid ${STYLE.clientsList}`}>
                <div className={STYLE.pageHeader}>
                    <h2>Clients</h2>
                </div>
                <div className={`_row`}>
                    <div className={`_col-12 _offset-md-8 _col-md-4 ${STYLE.searchBar}`}>
                        <SearchBar onClick={this.onSearch.bind(this)} placeholder={'Search'} />
                    </div>
                </div>
                <table>
                    <tbody>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Date of Bird</th>
                            <th>Action</th>
                        </tr>
                        {clientDOM}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default connect(mapState, mapDispatch)(Clients);
