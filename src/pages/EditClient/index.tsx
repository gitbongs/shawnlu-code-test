import * as React from 'react';
import {connect} from 'react-redux';
import ClientForm from './../../components/ClientForm';

import {updateClient, getClient} from "../../redux/action";

const STYLE = require('./style.scss');

const mapState = (state: any) => {
    return {
        client: state.currClient,
    };
};

const mapDispatch = (dispatch: any) => {
    return {
        updateClient: (id: string, formData: object) => {
            dispatch(updateClient(id, formData));
        },
        getClient(id: string) {
            dispatch(getClient(id));
        }
    };
};

interface IProps {
    getClient: any,
    history: any,
    match: any,
    client: any,
    default: any,
    updateClient: any,
}

type state = {
    firstName: "",
    lastName: "",
    dob: "",
}

class EditClient extends React.Component<IProps, state> {

    componentDidMount(): void {
        this.props.getClient(this.props.match.params.id);
    }

    onSubmit(formData: any) {
        console.log(formData);
        this.props.updateClient(this.props.match.params.id, formData);
        this.props.history.push('/clients');
    }

    render() {
        console.log(this.props.client);
        return (
            <div className={`gb-grid ${STYLE.addClient}`}>
                <div className={STYLE.pageHeader}>
                    <h2>Edit Client</h2>
                    <ClientForm default={this.props.client} onSubmit={this.onSubmit.bind(this)}/>
                </div>
            </div>
        )
    }
}

export default connect(mapState, mapDispatch)(EditClient);
