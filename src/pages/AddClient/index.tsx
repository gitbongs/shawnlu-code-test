import * as React from 'react';
import {connect} from 'react-redux';
import ClientForm from './../../components/ClientForm';

import {addClient, fetchClientData} from "../../redux/action";

const STYLE = require('./style.scss');

const mapState = (state: any) => {
    return {
        clients: state.clients,
        filterResult: state.filterResult,
    };
};

const mapDispatch = (dispatch: any) => {
    return {
        fetchClientData: () => {
            dispatch(fetchClientData());
        },
        addClient: (formData: any) => {
            dispatch(addClient(formData));
        },
    };
};

interface IProps {
    addClient: any,
    fetchClientData: any,
    history: any
}

type state = {
    firstName: "",
    lastName: "",
    dob: "",
}


class AddClient extends React.Component<IProps, state> {
    constructor(props: IProps) {
        super(props);
    }

    componentDidMount(): void {
        // this.props.fetchClientData();
    }

    onSubmit(formData: any) {
        this.props.addClient(formData);
        this.props.history.push('/clients');
    }

    render() {
        return (
            <div className={`gb-grid ${STYLE.addClient}`}>
                <div className={STYLE.pageHeader}>
                    <h2>Add Client</h2>
                    <ClientForm default={{}} onSubmit={this.onSubmit.bind(this)}/>
                </div>
            </div>
        )
    }
}

export default connect(mapState, mapDispatch)(AddClient);
