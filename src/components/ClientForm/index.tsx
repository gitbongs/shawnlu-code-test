import * as React from "react";

const STYLE = require('./style.scss');

interface IProps {
    default: any,
    onSubmit: any
}

type state = {
    first_name: "",
    last_name: "",
    date_of_birth: "",
}

class ClientForm extends React.Component<IProps, state> {
    componentDidMount(): void {
        console.log(this.props);
        // this.setState({
        //     first_name: this.props.default.first_name,
        //     last_name: this.props.default.last_name,
        //     date_of_birth: this.props.default.date_of_birth,
        // })
    }

    onSubmit(e: any) {

        this.props.onSubmit({
            first_name: this.state.first_name == undefined ? this.props.default.first_name : this.state.first_name,
            last_name: this.state.last_name == undefined ? this.props.default.last_name : this.state.last_name,
            date_of_birth: this.state.date_of_birth == undefined ? this.props.default.date_of_birth : this.state.date_of_birth,
        });
        e.preventDefault();
    }

    onInputFirstName(e: any) {
        this.setState({first_name: e.target.value});
    }

    onInputLastName(e: any) {
        this.setState({last_name: e.target.value});
    }

    onInputDOB(e: any) {
        this.setState({date_of_birth: e.target.value});
    }

    render() {
        let isReady = this.props.default != undefined;
        if (isReady) {
            return (
                <form action="" onSubmit={this.onSubmit.bind(this)} className={STYLE.clientForm}>
                    <div className={`_row ${STYLE.inputItem}`}>
                        <div className={`_col-12 _col-md-3 _col-lg-4`}>First Name:</div>
                        <div className={`_col-12 _col-md-6 _col-lg-4`}>
                            <input type="text"
                                   defaultValue={this.props.default.first_name}
                                   onChange={this.onInputFirstName.bind(this)}/>
                        </div>
                    </div>
                    <div className={`_row ${STYLE.inputItem}`}>
                        <div className={`_col-12 _col-md-3 _col-lg-4`}>Last Name:</div>
                        <div className={`_col-12 _col-md-6 _col-lg-4`}>
                            <input type="text"
                                   defaultValue={this.props.default.last_name}
                                   onChange={this.onInputLastName.bind(this)}/>
                        </div>
                    </div>

                    <div className={`_row ${STYLE.inputItem}`}>
                        <div className={`_col-12 _col-md-3 _col-lg-4`}>Date of Birth:</div>
                        <div className={`_col-12 _col-md-6 _col-lg-4`}>
                            <input type="text"
                                   defaultValue={this.props.default.date_of_birth}
                                   onChange={this.onInputDOB.bind(this)}/>
                        </div>
                    </div>

                    <div className={`_row ${STYLE.inputItem}`}>
                        <div className={`_col-12 _col-md-3 _col-lg-4`}></div>
                        <div className={`_col-12 _col-md-3 _col-lg-4  `}>
                            <button>Submit</button>
                        </div>
                    </div>
                </form>
            )
        } else {
            return (
                <div></div>
            )
        }
    }
}

export default ClientForm;


