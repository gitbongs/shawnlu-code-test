import * as React from 'react';

const STYLE = require('./style.scss');

interface ISearchBar {
    placeholder: string,
    onClick: any,
}

type state = {
    keyword: ''
}

class SearchBar extends React.Component<ISearchBar, state> {
    onInputing(e: any) {
        this.setState({keyword: e.target.value});
    }

    onSubmit(e: any) {
        this.props.onClick(this.state.keyword);
        e.preventDefault();
    }

    render() {
        return (
            <form action="" onSubmit={this.onSubmit.bind(this)}>
                <div className={STYLE.searchBar}>
                    <input onChange={this.onInputing.bind(this)} type="text" placeholder={this.props.placeholder}/>
                    <button onClick={this.onSubmit.bind(this)}>Search</button>
                </div>
            </form>
        )
    }
}

export default SearchBar;
