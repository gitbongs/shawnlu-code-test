import { EVENT } from './action';

const uuid = require('uuid/v4');

export default (state: any, action: any) => {
    switch (action.type) {
        case EVENT.FETCH_CLIENT_SUCCESS: {
            return {
                ...state,
                clients: { ...state.clients, data: action.payload.data },
                filterResult: { ...state.filterResult, data: action.payload.data }
            };
        }
        case EVENT.ON_FILTERING: {
            let clients = { ...state.clients };
            let result = clients.data.filter((item: any, i: number) => {
                let firstName = item.first_name.toLowerCase()
                let lastName = item.last_name.toLowerCase();
                let keyword = action.keyword.toLowerCase();
                return firstName.indexOf(action.keyword) != -1 || lastName.indexOf(keyword) != -1;
            });

            return {
                ...state,
                filterResult: {
                    ...state.filterResult,
                    data: result
                }
            }
        }

        case EVENT.ON_DELETE: {
            let clients = { ...state.clients };
            clients.data = clients.data.filter((item: any, i: number) => {
                return item.id != action.id;
            });
            let filterResult = { ...state.filterResult };
            filterResult.data = filterResult.data.filter((item: any, i: number) => {
                return item.id != action.id;
            });

            return { ...state, clients: { ...clients }, filterResult: { ...filterResult } };
        }

        case EVENT.ON_ADD_CLIENT: {
            state.clients.data.push({
                id: uuid(),
                first_name: action.formData.first_name,
                last_name: action.formData.last_name,
                date_of_birth: action.formData.date_of_birth,
            });
            return { ...state, filterResult: { ...state.filterResult, data: state.clients.data } };
        }

        case EVENT.ON_GET_CLIENT_BY_ID: {
            let clients = { ...state.clients };
            const client = clients.data.filter((item: any, i: string) => {
                return item.id == action.id
            });
            return { ...state, currClient: client[0] };
        }
        case EVENT.ON_UPDATE_CLIENT: {
            let clients = { ...state.clients };
            clients.data.map((item: any, i: string) => {
                if (item.id === action.id) {
                    item.first_name = action.formData.first_name;
                    item.last_name = action.formData.last_name;
                    item.date_of_birth = action.formData.date_of_birth;
                }
            });

            return { ...state, clients: clients, filterResult: clients };
        }
        case EVENT._RESET: {
            return { ...state, currClient: null };
        }
        default:
            return state;
    }
};
