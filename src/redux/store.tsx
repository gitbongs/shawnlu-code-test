import {createStore, applyMiddleware} from 'redux';
import reducer from './reducer';
import thunk from 'redux-thunk';


const initValue: object = {
    clients: {
        loading: false,
        data: [],
        updateAt: ''
    },
    filterResult: {
        data: [],
        updateAt: ''
    }
};

const store = createStore(reducer, initValue, applyMiddleware(thunk));
export default store;
