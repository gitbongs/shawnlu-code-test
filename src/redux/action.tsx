import axios from 'axios';

export const EVENT: any = {
    FETCH_CLIENT: 'FETCH_LAUNCHES',
    FETCH_CLIENT_SUCCESS: 'FETCH_CLIENT_SUCCESS',
    FETCH_CLIENT_FAIL: 'FETCH_CLIENT_FAIL',

    ON_FILTERING: 'ON_FILTERING',
    ON_DELETE: 'ON_DELETE',

    ON_ADD_CLIENT: 'ON_ADD_CLIENT',

    ON_GET_CLIENT_BY_ID: 'ON_GET_CLIENT_BY_ID',

    ON_UPDATE_CLIENT: 'ON_UPDATE_CLIENT',

    _RESET: '_RESET'
};

export const fetchClientData = () => (dispatch: any) => {
    dispatch({ type: EVENT.FETCH_CLIENT });
    return axios.get('/public/mock/clients.json').then((res: any) => {
        dispatch({ type: EVENT.FETCH_CLIENT_SUCCESS, payload: res.data })
    }).catch((err) => {
        dispatch({ type: EVENT.FETCH_CLIENT_FAIL, payload: err })
    })
};

export const filterClient = (val: string) => {
    return { type: EVENT.ON_FILTERING, keyword: val };
};

export const doDelete = (id: string) => {
    return { type: EVENT.ON_DELETE, id: id };
};

export const addClient = (formData: object) => {
    return { type: EVENT.ON_ADD_CLIENT, formData: formData };
};

export const getClient = (id: string) => {
    return { type: EVENT.ON_GET_CLIENT_BY_ID, id: id };
};
export const updateClient = (id: string, formData: object) => {
    return { type: EVENT.ON_UPDATE_CLIENT, id: id, formData };
};
export const _reset = () => {
    return { type: EVENT._RESET };
}

