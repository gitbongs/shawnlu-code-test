import * as React from 'react';
import { HashRouter as Router, Route, Link } from "react-router-dom";
import {connect} from 'react-redux';

import Layout from './layout/Layout';
import {addClient, fetchClientData} from "./redux/action";

import Clients from './pages/Clients';
import AddClient from './pages/AddClient';
import EditClient from './pages/EditClient';


const mapState = (state: any) => {
    return {
        clients: state.clients,
        filterResult: state.filterResult,
    };
};

const mapDispatch = (dispatch: any) => {
    return {
        fetchClientData: () => {
            dispatch(fetchClientData());
        },
        addClient: (formData: any) => {
            dispatch(addClient(formData));
        },
    };
};

interface IProps {
    addClient: any,
    fetchClientData: any,
    name: string,
    age: number
}


class App extends React.Component<IProps, {}> {
    constructor(props: IProps) {
        super(props);
    }

    componentDidMount(): void {
        this.props.fetchClientData();
    }

    onClick = (e: React.MouseEvent) => {
        console.log('click');
    }

    render() {
        return (
            <Router>
                <Layout>
                    <Route path="/clients" exact component={Clients}/>
                    <Route path="/addclient" exact component={AddClient}/>
                    <Route path="/client/:id" exact component={EditClient}/>
                </Layout>
            </Router>
        )
    }
}

export default connect(mapState, mapDispatch)(App);