import * as React from 'react';
import {Link} from "react-router-dom";


const STYLE = require('./style.scss');

interface IAppProps {
}

class Nav extends React.Component<IAppProps, {}> {
    constructor(props: IAppProps) {
        super(props);
    }

    render() {
        return (
            <nav className={`gb-grid _fluid ${STYLE.nav}`}>
                <span className={`${STYLE.logo}`}>LOGO</span>
                <div className={`${STYLE.navItems}`}>
                    <Link to="/clients">All Clients</Link>
                    <Link to="/addclient">Add Client</Link>
                </div>
            </nav>
        )
    }
}

export default Nav;