import * as React from 'react';
import { HashRouter as Router, Route, Link } from "react-router-dom";

import Nav from './Nav';
import { renderRoutes } from 'react-router-config';
import routesConfig from "../router/config";
import Clients from '../pages/Clients'
import AddClient from '../pages/AddClient'
import EditClient from '../pages/EditClient'

interface IProps {
    children: any
}

class Layout extends React.Component<IProps, {}> {
    constructor(props: IProps) {
        super(props);
    }

    render() {
        return (
            <div>
                <Nav />
                <div>
                    { this.props.children }
                </div>
            </div>
        )
    }
}

export default Layout;
