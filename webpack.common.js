var webpack = require("webpack");
const path = require('path');
const Dotenv = require('dotenv-webpack');
// const ManifestPlugin = require('webpack-manifest-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
	entry: {
		app: './src/index.tsx'
	},
	plugins: [
		new CleanWebpackPlugin(['dist']),
		new HtmlWebpackPlugin({
			// title: 'Production',
			template: './src/index.html',
			hash: true,
		}),
	],
	output: {
		filename: '[name].bundle.js',
		path: path.resolve(__dirname, 'dist'),
		publicPath: '/',
	},
	module: {
		rules: [
			{
				test: /\.js/,
				use: 'babel-loader',
				exclude: /node_modules/
			},
			{
				test: /\.tsx?$/,
				use: 'ts-loader',
				exclude: /node_modules/
			},
			{
				test: /\.css/,
				use: ['style-loader', 'css-loader']
			},
			{
				test: /\.scss/,
				use: [{
					loader: "style-loader" // 将 JS 字符串生成为 style 节点
				}, {
					loader: "css-loader", // 将 CSS 转化成 CommonJS 模块
					options: {
						modules: true,
						localIdentName: '[local]_[hash:base64:8]'
					}
				}, {
					loader: "sass-loader" // 将 Sass 编译成 CSS
				}]
			}
		]
	},
	resolve: {
		extensions: ['.tsx', '.ts', '.js']
	}
};