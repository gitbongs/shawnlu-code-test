var webpack = require("webpack");
const path = require('path');
const Dotenv = require('dotenv-webpack');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');

const MODE = 'development';

module.exports = merge(common, {
		mode: MODE,
		devtool: 'inline-source-map',
		devServer: {
			contentBase: path.join(__dirname, '/'),
			compress: true,
			// port: 8080,
			hot: true,
			open: false,
			hotOnly: true,
			clientLogLevel: "warning",
			publicPath: '/dist/'
		},
		plugins: [
			new webpack.HotModuleReplacementPlugin(),
			new Dotenv({
				path: path.resolve(`./.env.${MODE}`)
			}),
		],
	}
);